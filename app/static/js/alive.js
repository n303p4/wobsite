function secondsToHms(totalSeconds, showSeconds=false) {
    let hours = (Math.floor(totalSeconds / 3600) + "").padStart(2, "0");
    let minutes = ":" + (Math.floor(totalSeconds % 3600 / 60) + "").padStart(2, "0");
    let seconds = "";
    if (showSeconds) {
        seconds = ":" + (Math.floor(totalSeconds % 3600 % 60) + "").padStart(2, "0");
    }
    return `${hours}${minutes}${seconds}`;
}
function updateTimeElement(time, showSeconds=false) {
    let totalSeconds = parseFloat(time.getAttribute("total-seconds"));
    if (isNaN(totalSeconds)) {
        return;
    }
    totalSeconds += 1;
    time.setAttribute("total-seconds", totalSeconds);
    time.innerText = secondsToHms(totalSeconds, showSeconds);
}
function autoReload() {
    let seconds = new Date().getSeconds();
    setTimeout(autoReload, 1000);
    if (seconds % 10 === 2) {
        setTimeout(() => window.location.reload(), 500);
    }
    Array.from(document.querySelectorAll(".last-seen-ago")).forEach((time) => updateTimeElement(time, showSeconds=true));
}
autoReload();