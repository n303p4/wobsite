"""
wobsite

An overbearingly server-side website, where even dark theme is handled server-side
"""

# pylint: disable=invalid-name

import os
from datetime import datetime, timedelta
import multiprocessing as mp

from flask import Flask, abort, jsonify, make_response, redirect, render_template, request, session, url_for
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

app = Flask(__name__)
app.secret_key = os.environ.get("SECRET_KEY", os.urandom(64))
app.config["TEMPLATES_AUTO_RELOAD"] = True

limiter = Limiter(
    get_remote_address,
    app=app,
    default_limits=["15 per second"],
    storage_uri="memory://",
)

DOMAIN_NAME = os.environ.get("DOMAIN_NAME", "wob.site")
ALIVE_TOKENS = {}
# TODO: Replace with proper password hashing
# For now this is okay though due to the low threat model
with open("alive_tokens.txt", encoding="utf-8") as file_object:
    for line in file_object.read().split("\n"):
        try:
            hostname, token = line.split(":")
        except Exception:
            pass
        ALIVE_TOKENS[hostname] = token
assert ALIVE_TOKENS, "No tokens were set!"
manager = mp.Manager()
MACHINE_STATUSES = manager.dict()

LIGHT_THEME = ("#fefefe", "#454545")
DARK_THEME = ("#1a1a1a", "#c7c7c7")


def seconds_to_hms(total_seconds):
    hours, remainder = divmod(total_seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    return f"{hours:02.0f}:{minutes:02.0f}:{seconds:02.0f}"


def seconds_to_dhm(total_seconds):
    days, remainder = divmod(total_seconds, 86400)
    hours, remainder = divmod(remainder, 3600)
    minutes, seconds = divmod(remainder, 60)
    hours_minutes = f"{hours:02.0f}:{minutes:02.0f}"
    if days:
        return f"{days:.0f}d {hours_minutes}"
    return hours_minutes


def format_timedelta(td):
    return seconds_to_dhm(td.total_seconds())


def navbar_link(*, link_text: str = None):
    """Add an endpoint to the navbar."""
    def wrapper(function):
        app.config.setdefault("NAVBAR_LINKS", {})
        app.config["NAVBAR_LINKS"][function.__name__] = link_text or function.__doc__
        return function
    return wrapper


@navbar_link(link_text="Home")
@app.route("/")
def index():
    """The main endpoint."""
    return render_template(
        "index.jinja",
        title=DOMAIN_NAME
    )


def set_theme_contrast(theme, contrast):
    if theme not in ("auto", "light", "dark"):
        return "Invalid theme name: Must be auto, light, or dark", 400
    if theme == "auto":
        session["theme_name"] = None
    else:
        session["theme_name"] = theme
    if contrast.isdecimal():
        contrast = int(contrast)
    else:
        return "Invalid contrast value: Must be integer from 0 to 200", 400
    if contrast > 200:
        return "Invalid contrast value: Must be integer from 0 to 200", 400
    session["contrast"] = contrast
    if request.referrer:
        return redirect(request.referrer)
    return redirect(url_for("index"))


@app.route("/set-theme", methods=["POST"])
def set_theme():
    """Set contrast level."""
    theme = str(request.form.get("theme")).lower()
    contrast = str(request.form.get("contrast"))
    return set_theme_contrast(theme, contrast)


@app.route("/js")
def js():
    """Render JavaScript for session."""
    response = make_response(render_template("js.jinja"))
    response.headers["Content-Type"] = "text/javascript"
    return response


@app.route("/css")
def css():
    """Render CSS stylesheet for session."""
    response = make_response(render_template(
        "css.jinja",
        light_theme=LIGHT_THEME,
        dark_theme=DARK_THEME,
        theme_name=session.get("theme_name")
    ))
    response.headers["Content-Type"] = "text/css"
    return response


def alive_(hostnames, as_json: bool = False):
    """Machines that are alive."""
    if request.method in ("POST", "DELETE"):
        try:
            request_body = request.get_json()
        except Exception:
            abort(400)
        alive_token = request_body.get("token")
        if not alive_token:
            abort(401)
        alive_token = str(alive_token)
        hostname = request_body.get("hostName")
        if not hostname:
            abort(401)
        hostname = str(hostname)
        if not hostname in ALIVE_TOKENS:
            abort(401)
        if alive_token != ALIVE_TOKENS[hostname]:
            abort(401)
        if request.method == "POST":
            role = request_body.get("role")
            if role:
                role = str(role)
            else:
                role = ""
            uptime = request_body.get("uptime")
            details = request_body.get("details")
            if not isinstance(details, dict):
                details = None
            else:
                for key in details:
                    details[key] = str(details[key])
            MACHINE_STATUSES[hostname] = {
                "last_seen": datetime.utcnow(),
                "uptime": uptime,
                "role": role,
                "details": details
            }
        else:
            try:
                del MACHINE_STATUSES[hostname]
            except Exception:
                pass
        return f"{request.method} completed successfully!", 200
    machine_statuses = {}
    if not as_json:
        roles = []
        for hostname in hostnames:
            machine_status = MACHINE_STATUSES.get(hostname)
            if not machine_status:
                continue
            role = machine_status["role"]
            if role in roles:
                continue
            roles.append(role)
        roles.sort(key=lambda r: r.lower() if r else "z" * 32)
        for role in roles:
            machine_statuses.setdefault(role, {})
    now = datetime.utcnow()
    for hostname in hostnames:
        machine_status = MACHINE_STATUSES.get(hostname)
        if not machine_status:
            continue
        role = machine_status["role"]
        last_seen_ago = (now - machine_status["last_seen"]).total_seconds()
        last_seen_ago_string = seconds_to_hms(last_seen_ago)
        uptime = machine_status["uptime"]
        if isinstance(uptime, (int, float)):
            uptime_string = format_timedelta(timedelta(seconds=uptime))
        else:
            uptime_string = uptime
        if as_json:
            machine_statuses[hostname] = {
                "last_seen_ago": last_seen_ago,
                "last_seen_ago_string": last_seen_ago_string,
                "probably_dead": last_seen_ago > timedelta(minutes=5).total_seconds(),
                "uptime": uptime,
                "role": role,
                "uptime_string": uptime_string,
                "details": machine_status["details"]
            }
        else:
            machine_statuses[role][hostname] = {
                "last_seen_ago": last_seen_ago,
                "last_seen_ago_string": last_seen_ago_string,
                "probably_dead": last_seen_ago > timedelta(minutes=5).total_seconds(),
                "uptime": uptime,
                "uptime_string": uptime_string,
                "details": machine_status["details"]
            }
    if as_json:
        return machine_statuses
    return render_template(
        "alive.jinja",
        time_updated=f"{now.hour:02d}:{now.minute:02d}:{now.second:02d} UTC",
        machine_statuses=machine_statuses,
        title=DOMAIN_NAME
    )


@navbar_link(link_text="Machine statuses")
@app.route("/alive", methods=["GET", "POST", "DELETE"])
def alive():
    """Keep track of machine statuses."""
    return alive_(sorted(MACHINE_STATUSES.keys()))


@app.route("/alive.json")
def alive_json():
    """Keep track of machine statuses, as JSON."""
    return jsonify(alive_(sorted(MACHINE_STATUSES.keys()), as_json=True))


@app.route("/alive/<hostname>")
def alive_hostname(hostname):
    """Keep track of one machine status."""
    if hostname not in MACHINE_STATUSES:
        abort(404)
    return alive_([hostname])


@app.route("/alive/<hostname>.json")
def alive_hostname_json(hostname):
    """Keep track of one machine status."""
    if hostname not in MACHINE_STATUSES:
        abort(404)
    return jsonify(alive_([hostname], as_json=True)[hostname])


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=5000)
